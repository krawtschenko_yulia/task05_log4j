package me.krawtschenko.yulia;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    public static final String ACCOUNT_SID = "ACeb8e06ed966bb833f0a6255097b47116";
    public static final String AUTH_TOKEN = ${env::SMS_TOKEN};

    public static void send(String text) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        PhoneNumber from = new PhoneNumber("+13343800339");
        PhoneNumber to = new PhoneNumber("+380675483926");

        Message message = Message.creator(to, from, "Hello").create();
    }
}
