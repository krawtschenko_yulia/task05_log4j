package me.krawtschenko.yulia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(final String[] args) {
        logger.trace("Entering application.");

        logger.info("This is an info message.");
        logger.debug("This is a debugging message.");
        logger.warn("This is a warning.");
        logger.error("This is an error message.");
        logger.fatal("This is a fatal error message.");

        logger.trace("Exiting application.");
    }
}
